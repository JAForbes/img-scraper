#!/usr/bin/env node

const fs = require('fs')
const url = require('url')
const path = require('path')
const mime = require('mime')
const util = require('util')
const https = require('https')
const process = require('process')

const app = require('commander')
const bytes = require('bytes')
const jsdom = require('jsdom')
const srcset = require('srcset')
const sizeOf = util.promisify(require('image-size'))
const request = require('node-fetch')
const progress = require('cli-progress');
const platformFolders = require('platform-folders')

const picturesFolder = platformFolders.getPicturesFolder()

const DOM = $ => {
  const dom = new jsdom.JSDOM($)
  const window = dom.window
  const document = window.document
  const query = query => Array.from(document.querySelectorAll(query))

  return {
    dom, window, document, query
  }
}

async function Scraper(href, { follow }){

  const visited = {}
  const images = {}

  async function scrape(href){
    const { hostname, protocol } = url.parse(href)

    if( protocol != 'https:' ) {
      return;
    }

    let
    $ = await request(href).then( x => x.text() )

    $ = DOM($)

    const hrefs =
      follow
      ? $.query('a').map( x => url.resolve(href, x.href))
      .map( x => url.parse(x) )
      .filter( x => x.hostname == hostname )
      : []

    $.query('img').map(
      x => {
        const src =
          []
          .concat(
            x.getAttribute('data-srcset')
            ? [ srcset.parse(x.getAttribute('data-srcset'))
                .sort( (a,b) => b.width - a.width)[0]
              ]
              .map( x => x.url )
            : []
          )
          .concat(
            x.getAttribute('data-src')
            ? [x.getAttribute('data-src')]
            : []
          )
          .concat([ x.src ])
          .filter(Boolean)
          .shift()

        return url.resolve(href, src)
      }
    )
    .map( x => url.parse(x))
    .forEach( x => {
      images[x.href] = {
        ...x
        ,... path.parse(x.pathname)
        , seenOn: href
      }
    })

    for( let x of hrefs ) {
      if( !(x.href in visited) ) {
        visited[x.href] = x
        await scrape(x.href)
      }
    }

    return { visited, images }
  }

  return scrape(href)
}

function Downloader({
  targetDir
  , images
  , largestSideGreaterThan
  , filesizeGreaterThan
  , concurrency
  , onprogress=x=>x
}){

  const downloadStarted = {}
  const subdirCreated = {}
  const subdirCount = {}

  async function createSubDir(dir){
    if(dir in subdirCreated) {
      return;
    } else {
      await fs.promises.mkdir(dir, { recursive: true })
      subdirCreated[dir] = true
      return;
    }
  }

  async function downloadImage(image){
    const subdir = url.parse(image.seenOn).pathname
    const dir = path.resolve(targetDir, '.'+subdir)
    let filepath = path.resolve(dir, image.base)

    if( downloadStarted[filepath] ) {
      return;
    }

    subdirCount[dir] = subdirCount[dir] || 0
    subdirCount[dir]++
    downloadStarted[filepath] = true

    if(image.protocol != 'https:') {
      return;
    }

    await createSubDir(dir)

    await new Promise(
      (Y,N) => https.get(
        image.href, r => {
          const ext = '.' + mime.getExtension(r.headers['content-type'])
          if( path.extname(filepath) == '' ){
            filepath += ext
          }

          r.pipe(
            fs.createWriteStream( filepath )
              .on('error', N)
              .on('finish', Y)
              .on('end', Y)
          )
        }
      )
        .on('error', N)
    )



    const { width, height } = 
      await sizeOf(filepath)
      .catch( () => ({ width:0, height: 0 }) )

    const { size } = await fs.promises.stat(filepath)

    if(
      Math.max(width,height) < largestSideGreaterThan
      || size <= filesizeGreaterThan
    ){
      subdirCount[dir]--
      await fs.promises.unlink(filepath)
    }
  }

  async function downloadParallel(){

    const stack = Object.values(images)
    const total = stack.length
    let i = 0
    await new Promise(Y => {

      async function loop(){
        if( stack.length == 0 ){
          Y()
        } else if( stack.length > 0 ){
          const image = stack.pop()
          i++
          downloadImage(image)
            .catch( e => {
              console.error(e)
            } )
            .then( () => onprogress( Math.floor(i / total * 100) ) )
            .finally(loop)
        }
      }

      Array(concurrency).fill(0).forEach(loop)
    })

    await Promise.all(
      Object.entries(subdirCount)
        .filter(([, value]) => value == 0 )
        .map( ([key]) => fs.promises.rmdir(key) )
    )
  }

  async function downloadSerial(){

    for(let x of Object.values(images)){
      try {
        await downloadImage(x)
      } catch (e) {
        console.error(x, e)
      }
    }
  }

  const download = concurrency > 1
    ? downloadParallel
    : downloadSerial

  return download()
}

async function main({
  website: unnormalizedWebsite
  , minimumSize
  , minimumBytes
  , usePicturesFolder
  , dir=usePicturesFolder ? picturesFolder : process.cwd() 
  , concurrency=3
  , followAnchors: follow
}){

  const parsed = url.parse(unnormalizedWebsite)

  if( parsed.protocol == null ) {
    unnormalizedWebsite='https://'+unnormalizedWebsite
  } else if (parsed.protocol == 'http:' ) {
    unnormalizedWebsite=unnormalizedWebsite.replace('http', 'https')
  }
  website=unnormalizedWebsite

  const targetDir=path.resolve(dir, url.parse(website).host)
  const largestSideGreaterThan=minimumSize
  const filesizeGreaterThan=bytes.parse(minimumBytes)

  console.log('Writing to:',targetDir)
  console.log('Analyzing',website,'...')

  return Scraper(website, { follow })
    .then(
      async ({ images }) => {

        const bar = new progress.SingleBar({}, progress.Presets.shades_classic)
        bar.start(100, 0)

        await fs.promises.mkdir(targetDir, { recursive: true })
        await Downloader({
          targetDir
          , images
          , largestSideGreaterThan
          , filesizeGreaterThan
          , concurrency
          , onprogress(percent){
              bar.update(percent)
          }
        })
        bar.update(100)
        bar.stop()
      }
    )
}

app
  .description('Download all <img> on a website.')
  .option('-w, --website <url>', 'The entry point to the website.')
  .option('-c, --concurrency <n>', 'How many files to download simulaneously', 3)
  .option('-d, --dir <path>', 'The directory where the downloaded images will be saved.  Defaults to current directory')
  .option('-f, --follow-anchors', 'Will parse the initial site, then follow any same origin anchors and repeat the process.', false)
  .option('-p, --use-pictures-folder', 'Defaults the base path to the native OS pictures folder instead of the current working directory', false)
  .option('-s, --minimum-size <pixels>', 'The smallest the largest side of the image can be.  Smaller sizes will be deleted after download.', 1920)
  .option('-b, --minimum-bytes <bytes>', 'The minimum size in bytes for the file to be kept.  Smaller sizes will be deleted after download', '200KB')
  .parse(process.argv)

if( app.website ) {
  main(app)
    .catch(console.error)
} else {
  app.help()
}