img-scraper
-----------

#### Quick Start

`npx img-scraper -b 400 -s 0 -w https://unsplash.com/photos/G85VuTpw6jg -p`

#### Options

```
npx img-scraper --help
```

```
Usage: img-scraper [options]

Download all <img> on a website.

Options:
  -w, --website <url>          The entry point to the website.
  -c, --concurrency <n>        How many files to download simulaneously
                               (default: 3)
  -d, --dir <path>             The directory where the downloaded images will  
                               be saved.  Defaults to current directory        
  -f, --follow-anchors         Will parse the initial site, then follow any    
                               same origin anchors and repeat the process.     
                               (default: false)
  -p, --use-pictures-folder    Defaults the base path to the native OS
                               pictures folder instead of the current working  
                               directory (default: false)
  -s, --minimum-size <pixels>  The smallest the largest side of the image can
                               be.  Smaller sizes will be deleted after        
                               download. (default: 1920)
  -b, --minimum-bytes <bytes>  The minimum size in bytes for the file to be
                               kept.  Smaller sizes will be deleted after      
                               download (default: "200KB")
  -h, --help                   display help for command
```

#### Contributing

Yes, please.